Herbivorous:
* Horse
* Cow
* Sheep
* Frog
* Chicken

Carnivorous:
* Polar Bear
* Hawk
* Lion
* Fox
* Fennec Fox

Omnivorous 
* Cat
* Dog
* Rat
* Capybara
* Raccoon